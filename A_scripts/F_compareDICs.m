% plot DICs of different models

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';

% manually extracted DIC values from HDDM output (MRI session)
Model_null_EEG = 13525.60;
Model_v_EEG = 10598.5;
Model_va_EEG = 8281.7;
Model_tv_EEG = 6801.5;
Model_tvz_EEG = 6797.2;
Model_atv_EEG = 6116.3;

% manually extracted DIC values from HDDM output (MRI session)
Model_null_MRI = 9515.03;
Model_v_MRI = 7668.10;
Model_va_MRI = 5540.50;
Model_tv_MRI = 3839.00;
Model_atv_MRI = 3358.45;

h = figure('units','normalized','position',[.1 .1 .17 .4]); hold on;
    % first plot version with starting point variation underneath
    a = bar([Model_v_EEG-Model_null_EEG, Model_v_MRI-Model_null_MRI;...
        Model_va_EEG-Model_null_EEG, Model_va_MRI-Model_null_MRI;...
        Model_tvz_EEG-Model_null_EEG, Model_tv_MRI-Model_null_MRI;...
        Model_atv_EEG-Model_null_EEG, Model_atv_MRI-Model_null_MRI]);
    % then plot version without starting point variation overhead
    b = bar([Model_v_EEG-Model_null_EEG, Model_v_MRI-Model_null_MRI;...
        Model_va_EEG-Model_null_EEG, Model_va_MRI-Model_null_MRI;...
        Model_tv_EEG-Model_null_EEG, Model_tv_MRI-Model_null_MRI;...
        Model_atv_EEG-Model_null_EEG, Model_atv_MRI-Model_null_MRI]);
    ylabel({'Difference in Deviance Information Criterion (DIC)', 'from null model'})
    legend([b(1), b(2)],{'EEG'; 'MRI'}, 'orientation', 'horizontal', 'location', 'SouthEast'); legend('boxoff')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(a(1), 'FaceColor',[1 0 0])
    set(a(2), 'FaceColor',[.5 .5 .5], 'EdgeColor', [.5 .5 .5]);
    set(b(1), 'FaceColor',[0 0 0])
    set(b(2), 'FaceColor',[.5 .5 .5], 'EdgeColor', [.5 .5 .5]);
    grid minor; 
    curTicks = get(gca, 'ytick'); set(gca, 'ytick', curTicks(2:2:end));
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'+Drift', '+Drift,Thresh.', ...
        '+Drift, NDT', '+Drift,Thresh.,NDT'}, 'FontSize', 12);
    title({'HDDM model comparison'; ''}, 'FontSize', 16);
        
    % plot inset of starting point variation

    rectangle('Position',[2.7 -6800 3-2.7 -6600--6800], 'LineStyle', ':', 'LineWidth', 2)
    
    axes('Position',[.28 .15 .15 .19])
    
    box off
    hold on;
    % first plot version with starting point variation underneath
    a = bar([Model_v_EEG-Model_null_EEG, Model_v_MRI-Model_null_MRI;...
        Model_va_EEG-Model_null_EEG, Model_va_MRI-Model_null_MRI;...
        Model_tvz_EEG-Model_null_EEG, Model_tv_MRI-Model_null_MRI;...
        Model_atv_EEG-Model_null_EEG, Model_atv_MRI-Model_null_MRI]);
    % then plot version without starting point variation overhead
    b = bar([Model_v_EEG-Model_null_EEG, Model_v_MRI-Model_null_MRI;...
        Model_va_EEG-Model_null_EEG, Model_va_MRI-Model_null_MRI;...
        Model_tv_EEG-Model_null_EEG, Model_tv_MRI-Model_null_MRI;...
        Model_atv_EEG-Model_null_EEG, Model_atv_MRI-Model_null_MRI]);
    set(a(1), 'FaceColor',[1 0 0])
    set(a(2), 'FaceColor',[.5 .5 .5], 'EdgeColor', [.5 .5 .5]);
    set(b(1), 'FaceColor',[0 0 0])
    set(b(2), 'FaceColor',[.5 .5 .5], 'EdgeColor', [.5 .5 .5]);
    grid minor; 
    xlim([2.7 3]); ylim([-6800, -6600])
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'F_DICcomparison';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
