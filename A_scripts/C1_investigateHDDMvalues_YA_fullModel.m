% Calculate reliability of HDDM estimates

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
dataFile = [pn.root, 'B_data/EEG3/Data_v_a_t.csv'];
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
conditions = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesEEG = [];
MeanValuesEEG = NaN(numel(params), numel(conditions), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(conditions)
            strPattern=[params{indParam}, '_subj_',conditions{indCond},'_'];
            Indices = find(contains(ColumnNames,strPattern));
            EntryNum{indAge,indParam,indCond} = ...
                strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                'un',false), '_', '');
            EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
            MeanValuesEEG(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

% subjects are indexed with respect to IDs_all
pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'],'IDs_all');

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% plot mean effects on HDDM parameters

% h = figure('units','normalized','position',[.1 .1 .15 .6]);
% subplot(3,1,1); bar(squeeze(nanmean(MeanValuesEEG(1,:,idx_YA),3))); title('Drift rate')
% subplot(3,1,2); bar(squeeze(nanmean(MeanValuesEEG(2,:,idx_YA),3))); title('Threshold (boundary separation)')
% subplot(3,1,3); bar(squeeze(nanmean(MeanValuesEEG(3,:,idx_YA),3))); title('Non-decision time')

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [2.5, 1.75, 1.35];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.35, .45, .55];  
colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    hold on;
        dat = squeeze(permute(MeanValuesEEG(indIdx,:,idx_YA),[3,1,2]));
        bar(1:4, nanmean(dat), 'FaceColor',  colorm(indIdx,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % show standard deviation on top
        h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
        set(h1(1), 'marker', 'none'); % remove marker
        set(h1(2), 'LineWidth', 4);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
            'xlim', [0.5 4.5]); %ylim([-30*10^-8 20*10^-8])
        ylabel(paramLabels{indIdx}); xlabel('# of targets');
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel{indIdx}(indPair), pval);
            end
        end
end
set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_meanEffects_EEG_YA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% get MRI-based DDM values to investigate reliability

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
dataFile = [pn.root, 'B_data/MRI3/Data_v_a_t.csv'];
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1'};
conditions = {'1'; '2'; '3'; '4'};

Indices = []; EntryNum = []; MeanValuesMRI = [];
MeanValuesMRI = NaN(numel(params), numel(conditions), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond = 1:numel(conditions)
            strPattern=[params{indParam}, '_subj_',conditions{indCond},'_'];
            Indices = find(contains(ColumnNames,strPattern));
            EntryNum{indAge,indParam,indCond} = ...
                strrep(cellfun(@(x) x(end-2:end),ColumnNames(Indices),...
                'un',false), '_', '');
            EntryNum{indAge,indParam,indCond} = cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond},'un',0));
            MeanValuesMRI(indParam,indCond,EntryNum{indAge,indParam,indCond}) = nanmean(arrayData(:,Indices),1);
        end
    end
end
clear EntryNum Indices

%% plot mean effects on HDDM parameters

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

condPairs = [1,2; 2,3; 3,4];
condPairsLevel{1} = [2.5, 1.75, 1.35];  
condPairsLevel{2} = [1.6, 1.75, 2];  
condPairsLevel{3} = [.4, .5, .55];  
colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

h = figure('units','normalized','position',[.1 .1 .15 .6]);
for indIdx = 1:3
    subplot(3,1,indIdx)
    hold on;
        dat = squeeze(permute(MeanValuesMRI(indIdx,:,idx_YA),[3,1,2]));
        bar(1:4, nanmean(dat), 'FaceColor',  colorm(indIdx,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % show standard deviation on top
        h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
        set(h1(1), 'marker', 'none'); % remove marker
        set(h1(2), 'LineWidth', 4);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
            'xlim', [0.5 4.5]); %ylim([-30*10^-8 20*10^-8])
        ylabel(paramLabels{indIdx}); xlabel('# of targets');
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel{indIdx}(indPair), pval);
            end
        end
end
set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_meanEffects_MRI_YA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% collect DDM estimates in a structure

HDDM_summary.thresholdMRI = squeeze(permute(MeanValuesMRI(2,:,:),[3,2,1]));
HDDM_summary.nondecisionMRI = squeeze(permute(MeanValuesMRI(3,:,:),[3,2,1]));
HDDM_summary.driftMRI = squeeze(permute(MeanValuesMRI(1,:,:),[3,2,1]));

HDDM_summary.thresholdEEG = squeeze(permute(MeanValuesEEG(2,:,:),[3,2,1]));
HDDM_summary.nondecisionEEG = squeeze(permute(MeanValuesEEG(3,:,:),[3,2,1]));
HDDM_summary.driftEEG = squeeze(permute(MeanValuesEEG(1,:,:),[3,2,1]));

HDDM_summary.IDs = IDs_all;

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
save([dataPath, 'HDDM_summary_YA_vat.mat'], 'HDDM_summary')

