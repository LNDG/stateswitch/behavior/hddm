% check HDDM input data distributions (MRI and EEG superimposed)

EEGInput = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/StateSwitchDynamicTrialData_EEG_YA.mat');
MRIInput = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/StateSwitchDynamicTrialData_MRI_YA.mat');

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_YA_vat.mat')

% figure;
% imagesc(EEGInput.data)

% [commonIDs, iA, iB] = intersect(EEGInput.data_YA(:,7),MRIInput.data_YA(:,7));
commonIDs = unique(EEGInput.data_YA(:,7));

DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)),EEGInput.data_YA(:,7));
DDMIDidx = find(DDMIDidx);

countsEEG = []; countsMRI = [];
edges = 0:.05:2;
for indID = 1:numel(commonIDs)
    for indCond = 1:4
        countsEEG(indID,indCond,:) = histcounts(EEGInput.data_YA(EEGInput.data_YA(:,7) == commonIDs(indID) & EEGInput.data_YA(:,4) == indCond,3),edges);
    end
end

% figure;
% subplot(1,2,1); imagesc(squeeze(nanmean(countsEEG,1)))
% subplot(1,2,2); imagesc(squeeze(nanmean(countsMRI,1)))
% 
% figure;
% subplot(1,2,1); imagesc(edges, [], squeeze(nanmean(countsEEG,2)))
% subplot(1,2,2); imagesc(edges, [], squeeze(nanmean(countsMRI,2)))

% conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};

% figure
% for indCond = 1:4
%     subplot(1,4,indCond)
%     if indCond == 1
%         [~, sortIdx] = max(countsEEG(:,indCond,:), [], 3);
%         [~, sortIdx] = sort(sortIdx);
%     end
%     imagesc(edges, [], squeeze(countsEEG(sortIdx,indCond,:)))
%     hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),indCond),1:95, 'white', 'LineWidth', 2)
%     hold on; plot(HDDM_summary.thresholdEEG(DDMIDidx(sortIdx),indCond),1:95, 'red', 'LineWidth', 2)
%     %hold on; plot(HDDM_summary.driftEEG(DDMIDidx(sortIdx),indCond),1:95, 'yellow', 'LineWidth', 2)
%     title(['RT distributions ', conds{indCond}])
% end
% suptitle('Individual RT distributions sorted by Max bin in Load 1')
% colormap('hot')
% 
%% Figure SXX: sort by DDM-estimated NDT

conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};

h = figure('units','normalized','position',[.1 .1 .4 .4]);
indCount = 1;
for indCond = 1:3:4
    subplot(1,2,indCount)
    if indCond == 1
        [~, sortIdx] = sort(HDDM_summary.nondecisionEEG(DDMIDidx));
    end
    imagesc(edges, [], squeeze(countsEEG(sortIdx,indCond,:)))
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),1),1:47, 'red', 'LineWidth', 4, 'LineStyle', ':')
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),indCond),1:47, 'white', 'LineWidth', 4)
    title(['RT distributions ', conds{indCond}])
    xlabel('RT bin (s)'); ylabel('Subjects (sorted by NDT in Load 1)')
    indCount = indCount+1;
    cb = colorbar('location', 'SouthOutside'); set(get(cb,'ylabel'),'string','Amount of responses');
end
%suptitle('DDM: Individual RT distributions sorted by NDT in Load 1')
colormap('hot')
set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/C_figures/';
figureName = 'A3_NDT_RTdistribution';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% Figure: sort by DDM-estimated Threshold

conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};

figure
for indCond = 1:4
    subplot(1,4,indCond)
    %if indCond == 1
        [~, sortIdx] = sort(HDDM_summary.thresholdEEG(DDMIDidx,indCond));
    %end
    imagesc(edges, [], squeeze(countsEEG(sortIdx,indCond,:)))
    hold on; plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),indCond),1:42, 'white', 'LineWidth', 2)
    hold on; plot(HDDM_summary.thresholdEEG(DDMIDidx(sortIdx),indCond),1:42, 'red', 'LineWidth', 2)
    title(['RT distributions ', conds{indCond}])
    xlabel('RT bin (s)'); ylabel('Subjects (sorted by threshold)')
end
suptitle('DDM: Individual RT distributions sorted by threshold')
colormap('hot')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

%% Figure: sort by HDDM-estimated NDT

conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};

figure
for indCond = 1:4
    subplot(1,4,indCond)
    if indCond == 1
        [~, sortIdx] = sort(HHDDM_summary.nondecisionEEG(DDMIDidx));
    end
    imagesc(edges, [], squeeze(countsEEG(sortIdx,indCond,:)))
    hold on; plot(HHDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),indCond),1:95, 'white', 'LineWidth', 2)
    hold on; plot(HHDDM_summary.thresholdEEG(DDMIDidx(sortIdx),indCond),1:95, 'red', 'LineWidth', 2)
    %hold on; plot(HDDM_summary.driftEEG(DDMIDidx(sortIdx),indCond),1:95, 'yellow', 'LineWidth', 2)
    title(['RT distributions ', conds{indCond}])
    xlabel('RT bin (s)'); ylabel('Subjects (sorted by NDT in Load 1)')
end
suptitle('HDDM: Individual RT distributions sorted by NDT in Load 1')
colormap('hot')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

figure;
plot(HDDM_summary.nondecisionEEG(DDMIDidx(sortIdx),1))

splits = [1 23 46 69 95];

figure;
for indSplit = 1:4
    subplot(1,4,indSplit); hold on;
    for indCond = 1:4
        plot(squeeze(nanmean(countsEEG(sortIdx(splits(indSplit):splits(indSplit+1)),indCond,:),1)), 'LineWidth', 2);
    end
    title(['Quadriple split: Group ', num2str(indSplit)])
end
legend({'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'})
suptitle('Quadruple split does not suggest condition-differences in non-decision time')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure;
subplot(1,3,1)
imagesc(squeeze(countsEEG(sortIdx,2,:)-countsEEG(sortIdx,1,:))); title('Load 2-Load1'); xlabel('RTbin'); ylabel('Subejct, sorted by maxBin in Load1')
subplot(1,3,2)
imagesc(squeeze(countsEEG(sortIdx,3,:)-countsEEG(sortIdx,2,:))); title('Load 3-Load2'); xlabel('RTbin'); ylabel('Subejct, sorted by maxBin in Load1')
subplot(1,3,3)
imagesc(squeeze(countsEEG(sortIdx,4,:)-countsEEG(sortIdx,3,:))); title('Load 4-Load3'); xlabel('RTbin'); ylabel('Subejct, sorted by maxBin in Load1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,2,1); hold on;
    histogram(EEGInput.data(EEGInput.data(:,4) == 1 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 1,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 2 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 1,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 3 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 1,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 4 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 1,3))
    legend({'1 Target', '2 Targets', '3 Targets', '4 Targets'})
    title('YA Correct')
subplot(2,2,2); hold on;
    histogram(EEGInput.data(EEGInput.data(:,4) == 1 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 2,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 2 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 2,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 3 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 2,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 4 & EEGInput.data(:,2) == 1 & EEGInput.data(:,6) == 2,3))
    legend({'1 Target', '2 Targets', '3 Targets', '4 Targets'})
    title('OA Correct')
subplot(2,2,3); hold on;
    histogram(EEGInput.data(EEGInput.data(:,4) == 1 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 1,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 2 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 1,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 3 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 1,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 4 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 1,3))
    legend({'1 Target', '2 Targets', '3 Targets', '4 Targets'})
    title('YA False')
subplot(2,2,4); hold on;
    histogram(EEGInput.data(EEGInput.data(:,4) == 1 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 2,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 2 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 2,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 3 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 2,3))
    histogram(EEGInput.data(EEGInput.data(:,4) == 4 & EEGInput.data(:,2) == 0 & EEGInput.data(:,6) == 2,3))
    legend({'1 Target', '2 Targets', '3 Targets', '4 Targets'})
    title('OA False')
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/C_figures/';
figureName = 'A3_HDDM_RTdistributions';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% for each subject, get the amount of available trials per condition

pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'IDs_all');

for indID = 1:numel(IDs_all)
    for indCond = 1:4
        curTrials = find(EEGInput.data(:,7) == str2num(IDs_all{indID}) & EEGInput.data(:,4) ==indCond);
        TrialOverview(indID,indCond) = numel(curTrials);
        curTrials = find(MRIInput.data(:,7) == str2num(IDs_all{indID}) & MRIInput.data(:,4) ==indCond);
        TrialOverview(indID,4+indCond) = numel(curTrials);
    end
end

figure;
imagesc(TrialOverview); colorbar; title('Amount of available trials (EEG by cond, then MRI by Cond)');