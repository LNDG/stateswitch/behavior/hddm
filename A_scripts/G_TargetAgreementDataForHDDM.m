% bring data into HDDM format:
% Columns: condition (seperate subjects here), correct/incorrect, RTs (in s) 

clear all; clc;

pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
behavData = load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';

modalities = {'EEG'; 'MRI'};

for indModality = 1:numel(modalities)
    
    % get trials where two options (in two-state scenario) align
    % operationalized via the relative response agreement among the cues

    curField = ['MergedData', modalities{indModality}];
    
    UniqueResponses = NaN(256,102);

    for indID = 1:numel(behavData.(curField).expInfo) % loop across subjects
        if ~isempty(behavData.(curField).expInfo{indID})
            count = 1;
            for indRun = 1:4 % loop across runs
                for indBlock = 1:8 % loop across blocks
                    for indTrial = 1:8 % loop across trials
                        cuedAttributes = behavData.(curField).expInfo{indID}.AttCuesRun{indRun}{indBlock,indTrial};
                        cuedResponses = behavData.(curField).expInfo{indID}.HighProbChoiceRun{indRun}{indBlock,indTrial}(cuedAttributes);
                        targetResponse = behavData.(curField).expInfo{indID}.targetOptionRun{indRun}(indBlock,indTrial);
                        UniqueResponses(count,indID) = sum(ismember(cuedResponses, targetResponse))/numel(cuedResponses);
                        count = count + 1; % increase trial number;
                    end
                end
            end
        else
            UniqueResponses(:,indID) = NaN;
        end
    end

    %% get vectors of RTs and Acc

    RTs_vector = reshape(behavData.(curField).RTs,1,[]);
    Acc_vector = reshape(behavData.(curField).Accs,1,[]);
    Unique_vector = reshape(UniqueResponses,1,[]);
    
    [sortVal, sortInd] = sort(unique(Unique_vector(~isnan(Unique_vector))), 'ascend');
    
    for indVal = 1:numel(sortInd)
        Unique_vector_sorted(Unique_vector==sortVal(indVal)) = sortInd(indVal);
    end
    
    %% create vector of IDs

    Sub_vector = reshape(repmat(1:102,256,1),1,[]);

    %% create vector of age

    ageVec = [];
    IDs = cell2mat(behavData.IDs_all);
    IDs = str2mat(IDs(:,1));
    for indEntry = 1:numel(IDs)
        if strcmp(IDs(indEntry), '1')
            %ageVec{1,indEntry} = 'YA';
            ageVec(1,indEntry) = 1;
        elseif strcmp(IDs(indEntry), '2')
            %ageVec{1,indEntry} = 'OA';
            ageVec(1,indEntry) = 2;
        end
    end; clear IDs;

    Age_vector = reshape(repmat(ageVec,256,1),1,[]);

    %% create vector of dimensionality

    Dim_vector = reshape(behavData.(curField).StateOrders,1,[]);

    %% create vector of attribute

    Att_vector = reshape(behavData.(curField).Atts,1,[]);

    %% combine in data matrix

    data = [];
    data = [Sub_vector', Acc_vector', RTs_vector', Unique_vector_sorted', Dim_vector', Att_vector', Age_vector'];

    %% set any RTs below 250 ms to NaN

    data(find(RTs_vector<=.25),:) = NaN;

    %figure; plot(sort(RTs_vector,'ascend')); ylim([0 .25])

    %% delete any rows with nans

    data(any(isnan(data), 2), :) = [];
    
    %% create 2 alternate versions: (1) within L4: by agreement (2) across loads, for agreement = 1
%     
%     data(:,4) == 1;

    %% add complete ID

    IDs_mat =  cellfun(@str2num,behavData.IDs_all);
    data(:,8) = IDs_mat(data(:,1)); clear IDs_mat;

    %% export data matrix

    data_YA = [data(data(:,7)<2000,:)];
    data_OA = [data(data(:,7)>=2000,:)];
    %save([pn.dataOut, 'StateSwitchDynamicTrialData_',modalities{indModality},'.mat'],'data')
    %save([pn.dataOut, 'StateSwitchDynamicTrialData_',modalities{indModality},'_YA_TargetAgreement.mat'],'data_YA')
    %save([pn.dataOut, 'StateSwitchDynamicTrialData_',modalities{indModality},'_OA.mat'],'data_OA')

    %% add headers

    data = num2cell(data);
    data = [{'subject'},{'acc'},{'rt'},{'agree'},{'dim'},{'att'},{'age'},{'ID'};data];

    %% export data matrix as .csv

%     addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/cell2csv/')
%     cell2csv([pn.dataOut, 'StateSwitchDynamicTrialData_',modalities{indModality},'.dat'],data)

    %% create separate structures for younger and older adults

    fields = cellfun(@(x)(x>=2000), data(:,8), 'UniformOutput', 0);
    OAinds = cell2mat(fields(2:end));

    data_YA = [data(1,:); data(find(OAinds==0)+1,:)];
    %data_OA = [data(1,:); data(find(OAinds==1)+1,:)];

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/cell2csv/')
    cell2csv([pn.dataOut, 'StateSwitchDynamicTrialData_',modalities{indModality},'_YA_TargetAgreement.dat'],data_YA)
    %cell2csv([pn.dataOut, 'StateSwitchDynamicTrialData_',modalities{indModality},'_OA.dat'],data_OA)
end